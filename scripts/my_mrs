#!/bin/bash

two_days_ago=$(date '+%Y-%m-%dT%H:%M:%SZ' -d '2 days ago')
GLAB_UID=${GLAB_UID:-$(glab api user | jq -r .id)}
cmd_path=$(realpath $0)
CACHED_MR_JSON=${CACHED_MR_JSON:-""}

GLAB=${GLAB:-glab}
GLFLOW=${GLFLOW:-""}
if [[ -n "${MILESTONE}" && -z "${MILESTONES}" ]]; then
	MILESTONES="${MILESTONE}"
elif [[ -n "${MILESTONE}" && -n "${MILESTONES}" ]]; then
	MILESTONES="${MILESTONES},${MILESTONE}"
fi

if [ -n "${STALE}" ]; then
	FROM_DATE=${two_days_ago}
else
	FROM_DATE=${FROM_DATE:-""}
fi

filter_labels() {
	if [ -z "${LABELS}" ]; then
		cat
	else
		local _labels_exp
		local _ifs=${IFS}
		local _l
		IFS=","
		for _l in ${LABELS}; do
			_labels_exp=${_labels_exp}${_labels_exp:+","}"\"${_l}\""
		done
		jq --argjson filter "[${_labels_exp}]" '[.[] | .labels as $labels | select($labels | contains($filter))]'
	fi
}

highlight_labels() {
	if [ -z "${HIGHLIGHT_LABELS}" ]; then
		cat
	else
		local _labels_exp
		local _ifs=${IFS}
		local _l
		IFS=","
		for _l in ${HIGHLIGHT_LABELS}; do
			_labels_exp=${_labels_exp}${_labels_exp:+","}"\"${_l}\""
		done
		IFS=$_ifs
		local _my_json=$(cat)
		local _expanded_labels=$(echo "$_my_json" | jq --argjson highlights "[${_labels_exp}]" '[.[] | .labels as $labels | foreach $highlights[] as $highlight (0; $labels[] | select(. | match($highlight)))] | sort | unique')
		echo "$_my_json" | jq --argjson highlights "${_expanded_labels}" '[.[] | .labels as $labels | (.labels |= $labels-($labels-$highlights))]'
	fi
}

json() {
	# json_full | jq '[.[] | {"title": .title, "updated": .updated_at, "url": .web_url, "milestone": .milestone.title}]'
	json_full | jq '[.[] | .references.full as $reference | {"title": .title, "type": "MergeRequest", "source": "MergeRequest", "updated": .updated_at, "url": .web_url, "milestone": .milestone.title, "labels": .labels, "reference": $reference, "project": ($reference | split("!"))[0]}]'
}

text() {
	if [ -t 1 -o "$COLOR" == "yes" ]; then
		_coloredtext
	else
		_plaintext
	fi
}
_plaintext() {
	local _jq_query

	if [ -n "${HIGHLIGHT_LABELS}" ]; then
		_jq_query='.[] | "\(.updated_at) \(.milestone.title) \(.title) \(.labels|map("~"+.)|join(",")) \(.web_url)"'
	else
		_jq_query='.[] | "\(.updated_at) \(.milestone.title) \(.title) \(.web_url)"'
	fi

	json_full |
		jq \
			-r "$_jq_query"
}

_coloredtext() {
	local _lightblue=$'\u001b[1;34m'
	local _reset=$'\u001b[00m'
	local _darkgray=$'\u001b[1;30m'
	local _blue=$'\u001b[0;34m'
	local _darkgreen=$'\u001b[0;32m'

	local _jq_query

	if [ -n "${HIGHLIGHT_LABELS}" ]; then
		_jq_query='.[] | $darkgray+"\(.updated_at) "+$lightblue+"\(.milestone.title) "+$reset+"\(.title) "+$darkgreen+"\(.labels|map("~"+.)|join(","))"+$blue+" \(.web_url)"+$reset'
	else
		_jq_query='.[] | $darkgray+"\(.updated_at) "+$lightblue+"\(.milestone.title) "+$reset+"\(.title) "+$blue+"\(.web_url)"+$reset'
	fi

	json_full |
		jq \
			--arg lightblue "$_lightblue" \
			--arg darkgray "$_darkgray" \
			--arg reset "$_reset" \
			--arg blue "$_blue" \
			--arg darkgreen "$_darkgreen" \
			-r "${_jq_query}"
}

json_full() {
	local _cmd
	if [[ -n "${CACHED_MR_JSON}" ]] && [[ -r "${CACHED_MR_JSON}" ]]; then
		cat ${CACHED_MR_JSON}
	fi
	if [[ -n "${GLFLOW}" ]]; then
		_cmd="json_full_glflow"
	else
		_cmd="json_full_glab"
	fi

	if [ "${MILESTONE+set}" == "set" -a -z "${MILESTONE}" -a -z "${MILESTONES}" ]; then
		$_cmd | jq '[ .[] | select(.milestone == null)]'
	elif [ -z "${MILESTONE}" -a -z "${MILESTONES}" ]; then
		$_cmd
	else
		OLD_IFS="$IFS"
		IFS=","
		for MILESTONE in ${MILESTONES}; do
			IFS="${OLD_IFS}" $_cmd
		done | jq -s '. | flatten(2)'
		IFS="${OLD_IFS}"
	fi
}

json_full_glflow() {
	local _milestone=""
	local _beforedate=""
	_milestone=${MILESTONE:+", \"milestone\": \"${MILESTONE}\""}
	_beforedate=${FROM_DATE:+", \"updated_before\": \"${FROM_DATE}\""}
	${GLFLOW} api --query-data "{ \"assignee_id\": \"${GLAB_UID}\"${_beforedate}, \"scope\": \"all\", \"state\": \"opened\"${_milestone} }" /merge_requests | filter_labels | highlight_labels
}

json_full_glab() {
	local _milestone=""
	local _beforedate=""
	_milestone=${MILESTONE:+"&milestone=${MILESTONE}"}
	_milestone=$(echo "${_milestone}" | tr " " "+")
	_beforedate=${FROM_DATE:+"&updated_before=${FROM_DATE}"}
	glab api --paginate "/merge_requests?assignee_id=${GLAB_UID}${_beforedate}&scope=all&state=opened${_milestone}" |
		jq -rn '[inputs|.[]]' |
		filter_labels |
		highlight_labels
}

open() {
	json_full | jq -r '.[].web_url' | xargs xdg-open-all
}

help() {
	echo "Available commands:"
	# quotes in the middle of expressions for skipping over self
	grep -F '(''){' $cmd_path | sed -e 's/(''){//g; s/^/  /g'
}

if [ $# -lt 1 ]; then
	json
else
	for cmd in $@; do
		$cmd
	done
fi
