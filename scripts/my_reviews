#!/bin/bash

two_days_ago=$(date '+%Y-%m-%dT%H:%M:%SZ' -d '2 days ago')
GLAB_UID=${GLAB_UID:-$(glab api user | jq -r .id)}
GLAB_EMAIL=${GLAB_EMAIL:-$(glab api user | jq -r .email)}
cmd_path=$(realpath $0)
#
GLFLOW=${GLFLOW:-""}
GLAB=${GLAB:-glab}

TMP_FILES=""
APPROVED_MRS=""
cleanup() {
	echo "Cleaning up ${TMP_FILES}" 1>&2
	for f in ${TMP_FILES}; do
		[ -n "${DEBUG:+x}" ] && rm -f ${f} || true
	done
	TMP_FILES=""
}

trap cleanup EXIT RETURN

if [[ -n "${MILESTONE}" && -z "${MILESTONES}" ]]; then
	MILESTONES="${MILESTONE}"
elif [[ -n "${MILESTONE}" && -n "${MILESTONES}" ]]; then
	MILESTONES="${MILESTONES},${MILESTONE}"
fi
#echo "$MILESTONE"
#echo "$MILESTONES"

if [ -n "${STALE}" ]; then
	FROM_DATE=${two_days_ago}
else
	FROM_DATE=${FROM_DATE:-""}
fi

filter_labels() {
	if [ -z "${LABELS}" ]; then
		cat
	else
		local _labels_exp
		local _ifs=${IFS}
		local _l
		IFS=","
		for _l in ${LABELS}; do
			_labels_exp=${_labels_exp}${_labels_exp:+","}"\"${_l}\""
		done
		jq --argjson filter "[${_labels_exp}]" '[.[] | .labels as $labels | select($labels | contains($filter))]'
	fi
}

highlight_labels() {
	if [ -z "${HIGHLIGHT_LABELS}" ]; then
		cat
	else
		local _labels_exp
		local _ifs=${IFS}
		local _l
		IFS=","
		for _l in ${HIGHLIGHT_LABELS}; do
			_labels_exp=${_labels_exp}${_labels_exp:+","}"\"${_l}\""
		done
		IFS=$_ifs
		local _my_json=$(cat)
		local _expanded_labels=$(echo "$_my_json" | jq --argjson highlights "[${_labels_exp}]" '[.[] | .labels as $labels | foreach $highlights[] as $highlight (0; $labels[] | select(. | match($highlight)))] | sort | unique')
		echo "$_my_json" | jq --argjson highlights "${_expanded_labels}" '[.[] | .labels as $labels | (.labels |= $labels-($labels-$highlights))]'
	fi
}

filter_approvals() {
	if [ "${APPROVED}" == "yes" ]; then
		cat
		return
	fi
	local _json_file=$(mktemp --tmpdir --suffix=.json reviews.XXX)
	TMP_FILES="${TMP_FILES:+" "} ${_json_file}"
	local _project_id
	local _project_id_pre
	local _mr_id
	local _mr_iid
	local _in_approvers
	local _mrs_exp
	local _approved_mrs
	cat >${_json_file}
	for _mr in $(jq -r '.[] | "\(.iid):\(.project_id):\(.id)\n"' ${_json_file}); do
		# echo "=> ${_mr}"
		_mr_iid=${_mr%%:*}
		_mr_id=${_mr##*:}

		_project_id_pre=${_mr#*:}
		_project_id=${_project_id_pre%:*}
		# echo "// ${_project_id}:${_mr_id} approvals:" 1>&2
		_in_approvers=$(${GLAB} api --paginate /projects/${_project_id}/merge_requests/${_mr_iid}/approval_state |
			jq -r --arg uid ${GLAB_UID} \
				'.rules[].approved_by | [.[].id] as $ids | .[] | select($ids | contains([$uid | tonumber])) | .id')
		if [[ -n "${_in_approvers}" ]]; then
			# Already approved
			# echo "// Already reviewed id ${_mr_id}" 1>&2
			_approved_mrs=${_mr_id}${_approved_mrs:+","}${_approved_mrs}
		else
			# not approved yet
			_mrs_exp=${_mr_id}${_mrs_exp:+","}${_mrs_exp}
		fi
	done
	# Set up global for further use
	APPROVED_MRS=${_approved_mrs}
	jq --argjson filter \
		"[${_mrs_exp}]" '[.[] | .id as $id | select($filter | contains([$id]))]' \
		${_json_file}
}

json() {
	json_full | jq '[.[] | .references.full as $reference | {"title": .title, "type": "MergeRequest", "source": "Review", "updated": .updated_at, "url": .web_url, "milestone": .milestone.title, "labels": .labels, "reference": $reference, "project": ($reference | split("!"))[0]}]'
}

text() {
	if [ -t 1 -o "$COLOR" == "yes" ]; then
		_coloredtext
	else
		_plaintext
	fi
}

_plaintext() {
	local _jq_query

	if [ -n "${HIGHLIGHT_LABELS}" ]; then
		_jq_query='.[] | "\(.updated_at) \(.milestone.title) \(.title) \(.labels|map("~"+.)|join(",")) \(.web_url)"'
	else
		_jq_query='.[] | "\(.updated_at) \(.milestone.title) \(.title) \(.web_url)"'
	fi
	json_full |
		jq \
			-r "$_jq_query"
}

_coloredtext() {
	local _lightblue=$'\u001b[1;34m'
	local _reset=$'\u001b[00m'
	local _darkgray=$'\u001b[1;30m'
	local _blue=$'\u001b[0;34m'
	local _darkgreen=$'\u001b[0;32m'

	local _jq_query

	if [ -n "${HIGHLIGHT_LABELS}" ]; then
		_jq_query='.[] | $darkgray+"\(.updated_at) "+$lightblue+"\(.milestone.title) "+$reset+"\(.title) "+$darkgreen+"\(.labels|map("~"+.)|join(","))"+$blue+" \(.web_url)"+$reset'
	else
		_jq_query='.[] | $darkgray+"\(.updated_at) "+$lightblue+"\(.milestone.title) "+$reset+"\(.title) "+$blue+"\(.web_url)"+$reset'
	fi

	json_full |
		jq \
			--arg lightblue "$_lightblue" \
			--arg darkgray "$_darkgray" \
			--arg darkgreen "$_darkgreen" \
			--arg reset "$_reset" \
			--arg blue "$_blue" \
			-r "$_jq_query"
}

json_full() {
	local _cmd
	if [[ -n "${GLFLOW}" ]]; then
		_cmd="json_full_glflow"
	else
		_cmd="json_full_glab"
	fi

	if [ "${MILESTONE+set}" == "set" -a -z "${MILESTONE}" ]; then
		$_cmd | jq '[ .[] | select(.milestone == null)]'
	elif [ -z "${MILESTONE}" -a -z "${MILESTONES}" ]; then
		$_cmd
	else
		OLD_IFS="$IFS"
		IFS=","
		for MILESTONE in ${MILESTONES}; do
			IFS=${OLD_IFS} $_cmd
		done | jq -s '. | flatten(2)'
		IFS="${OLD_IFS}"
	fi
}

json_full_glab() {
	local _milestone=""
	local _beforedate=""
	_milestone=${MILESTONE:+"&milestone=${MILESTONE}"}
	_beforedate=${FROM_DATE:+"&updated_before=${FROM_DATE}"}
	${GLAB} api --paginate "/merge_requests?reviewer_id=${GLAB_UID}${_beforedate}&scope=all&state=opened${_milestone}" |
		jq -rn '[inputs|.[]]' |
		filter_labels |
		filter_approvals |
		highlight_labels
}

json_full_glflow() {
	local _milestone=""
	local _beforedate=""
	_milestone=${MILESTONE:+", \"milestone\": \"${MILESTONE}\""}
	_beforedate=${FROM_DATE:+", \"updated_before\": \"${FROM_DATE}\""}
	${GLFLOW} api --query-data "{ \"reviewer_id\": \"${GLAB_UID}\"${_beforedate}, \"scope\": \"all\", \"state\": \"opened\"${_milestone} }" /merge_requests | filter_labels | filter_approvals | highlight_labels
}

open() {
	json_full | jq -r '.[].web_url' | xargs xdg-open-all
}

help() {
	echo "Available commands:"
	# quotes in the middle of expressions for skipping over self
	grep -F '(''){' $cmd_path | sed -e 's/(''){//g; s/^/  /g'
}

if [ $# -lt 1 ]; then
	json
else
	for cmd in $@; do
		$cmd
	done
fi
