# Activity report scripts

This project is designed to provide weekly reports of past and planned activity with not much data preservation (you'd have to copy-paste results into some more permanent location or add to pipeline steps for preservation)

## Features

* report activity for specified period
* show planned activity for pre-defined set of Milestones
* list watched activities (using "reaction emoji hack")

## Getting started

* Fork the project
* make sure Pages are enabled for the fork
* make sure `CI/CD` is enabled in the fork
* under the `Settings / CI/CD / Variables` add `GITLAB_URL` variable to point to desired GitLab instance, `GITLAB_PRIVATE_TOKEN` containing PAT allowing read-only access to `GITLAB_URL`
* Run pipeline using `FROM_DATE` and `TO_DATE` variables. Use `MILESTONES` to list milestones to check for WorkPlan section in addition to one calculated by `scripts/current_milestone`
  * report will be accessible under Pages URL or as an artifact of `pages` job (`public/report.html`)

## Running locally

Quite a few scripts from this collection are designed to be run locally to provide immediate tactical view of current activity (esp. work planning). All of `my*` scripts are designed to provide a view on planned work for specified milestones. 

### Requirements
* Scripts utilize [glab](https://gitlab.com/gitlab-org/cli) and `gl-flow.py` from [gl-workflow](https://gitlab.com/dmakovey/gl-workflow) so those requirements will have to be locally installed prior to execution of scripts.
  * Currently I'm in process of removing `gl-flow.py` dependency in favour of `glab` but there are some nuances and issues associated with that transition.
* Environment variables `GITLAB_TOKEN` and `GITLAB_PRIVATE_TOKEN` will need to be set up and exported prior to execution.
* `my` script also requires setup of [runctl](https://github.com/droopy4096/runctl) to manage various configurations through environment variables combinations and is a convenience wrapper around `my_*` set of scripts.
